# Flutter TDD

A flutter sample project for TDD and clean architecture design

## Architecture Layers

* **Presentation**
    *  Widgets
    *  Pages
    *  Bloc
* **Domain**
    * Repositories
    * Entities
    * UseCases
* **Data**
    * Models
    * DataSources
    * Repositories (Impl)

## Dependencies (Dart packages)

**Service locator:**
* get_it: ^2.0.1

**Bloc for state management:**
* flutter_bloc: ^0.21.0

**Value equality:**
* equatable: ^0.4.0

**Functional programming:**
* dartz: ^0.8.6

**Remote api:**
* data_connection_checker: ^0.3.4
*   http: ^0.12.0+2

**Local cache:**
* shared_preferences: ^0.5.3+4
