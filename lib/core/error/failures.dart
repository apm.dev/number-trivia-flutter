import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  Failure([List propertise = const <dynamic>[]]) : super(propertise);
}

// General failures
class ServerFailure extends Failure {}

class CacheFailure extends Failure {}
