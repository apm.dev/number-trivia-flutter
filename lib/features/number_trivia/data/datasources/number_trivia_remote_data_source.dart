import '../models/number_trivia_model.dart';

abstract class NumberTriviaRemoteDataSource {
  /// Calls http://localhost/trivia/{number} endpoint.
  ///
  /// Throws a [ServerExeption] for all error codes.
  Future<NumberTriviaModel> getConcreteNumberTrivia(int number);

  /// Calls http://localhost/trivia/ endpoint.
  ///
  /// Throws a [ServerExeption] for all error codes.
  Future<NumberTriviaModel> getRandomNumberTrivia();
}
