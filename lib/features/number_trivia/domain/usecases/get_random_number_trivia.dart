import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';

import '../repositories/number_trivia_repository.dart';
import '../entities/number_trivia.dart';
import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';

class GetRandomNumberTrivia implements UseCase<NumberTrivia, NoParams> {
  final NumberTriviaRepository repository;

  GetRandomNumberTrivia({@required this.repository});

  @override
  Future<Either<Failure, NumberTrivia>> call(NoParams params) async {
    return await repository.getRandomNumber();
  }
}
