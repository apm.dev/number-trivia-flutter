import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primarySwatch: Colors.green),
      home: Scaffold(
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.adb),
          onPressed: () {
            print('Slm..');
          },
        ),
        appBar: AppBar(
          title: Text('ResoCoder'),
          centerTitle: true,
        ),
        body: Center(
          child: Container(
            child: IconButton(
              iconSize: 48,
              color: Colors.blueGrey,
              splashColor: Colors.yellowAccent.shade700,
              highlightColor: Colors.transparent,
              icon: Icon(Icons.account_balance_wallet),
              onPressed: () {},
            ),
          ),
        ),
      ),
    );
  }
}
