import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../../lib/features/number_trivia/domain/entities/number_trivia.dart';
import '../../../../../lib/features/number_trivia/domain/usecases/get_concrete_number_trivia.dart';
import '../../../../../lib/features/number_trivia/domain/repositories/number_trivia_repository.dart';

class MockNumberTriviaRepository extends Mock
    implements NumberTriviaRepository {}

void main() {
  GetConcreteNumberTrivia usecase;
  MockNumberTriviaRepository mockNumberTriviaRepository;

  setUp(() {
    mockNumberTriviaRepository = MockNumberTriviaRepository();
    usecase = GetConcreteNumberTrivia(mockNumberTriviaRepository);
  });

  final tNumber = 1;
  final tNumberTrivia = NumberTrivia(number: 4, text: 'test');

  test(
    'Should get trivia for the number from the repository',
    () async {
      //arrange
      when(mockNumberTriviaRepository.getConcreteNumber(any))
          .thenAnswer((_) async => Right(tNumberTrivia));
      //act
      final result = await usecase(Params(tNumber));
      //assert
      expect(result, Right(tNumberTrivia));
      verify(mockNumberTriviaRepository.getConcreteNumber(tNumber));
      verifyNoMoreInteractions(mockNumberTriviaRepository);
    },
  );
}
